package exam;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;

public class S2 {

	public static void main(String[] args) {
		MThread n1  = new MThread();
		MThread n2 = new MThread();
		Thread t1 = new Thread(n1);
		Thread t2 = new Thread(n2);
		t1.setName("MThread1");
		t2.setName("MThread2");
		t1.start();
		t2.start();
	

	}

}


class MThread implements Runnable{

	@Override
	public void run() {
		int i;
		for(i=0;i<3;i++) {
			 DateFormat df = new SimpleDateFormat("dd/MM/yy HH:mm:ss");
			 Date d = new Date();
			System.out.println(Thread.currentThread().getName()+" "+df.format(d));
			try {
				Thread.sleep(7000);
			} catch (InterruptedException e) {
			
				e.printStackTrace();
			}
		}
		
	}
	
}
