package exam;




public class I extends U{
	private long t;
	private K k;
	
	public void f() {
		System.out.println("f");
	}
	
	public void i(J j) {
		j.j();
	}
	
	public void setK(K k) {
		this.k=k;
	}
	
	

	public static void main(String[] args) {
	I i = new I();
	i.f();

	}

}

 class U{
	 
	 public void p() {
		 System.out.println("p");
	 }
	
}
 
 class J{
	 public void j() {
		 System.out.println("j");
	 }
 }
 
 class N{
	 private I i;
	 public N(I i) {
		 this.i = i;
		 
	 }
 }
 
 class K{
	 public void k() {
		 System.out.println("k");
	 }
	 
	 class L{
		 public void metA() {
			 System.out.println("A");
		 }
	 }
 }
 
 class S{
	 private K k;
	 public S(K k) {
		 this.k=k;
	 }
	 public void metB() {
		 System.out.println("B");
		 
	 }
	 
 }
